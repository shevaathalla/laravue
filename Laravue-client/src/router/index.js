import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "product.index",
    component: () => import("../views/product/index.vue"),
  },
  {
    path: "/create",
    name: "product.create",
    component: () => import("../views/product/create.vue"),
  },
  {
    path: "/edit/:id",
    name: "product.edit",
    component: () => import("../views/product/edit.vue"),
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../views/about.vue"),
  },
];

const router = createRouter({ history: createWebHistory(), routes });

export default router;
