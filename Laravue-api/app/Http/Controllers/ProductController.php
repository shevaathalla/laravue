<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'DESC')->get();
        $response = [
            'message' => 'Product indexed by created time',
            'data' => $products,
        ];

        return response()->json($response, Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required'],
            'detail' => ['required'],
            'stock' => ['required', 'numeric'],
            'type' => ['required', 'in:soft,hard'],
        ]);
        if ($validation->fails()) {
            return response()->json($validation->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        try {
            $product = Product::create($request->all());
            $response = [
                'message' => 'Product created!',
                'data' => $product,
            ];

            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed : " . $e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        $response = [
            'message' => 'Product Found',
            'data' => $product,
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        
        $validation = Validator::make($request->all(), [
            'name' => ['required'],
            'detail' => ['required'],
            'stock' => ['required', 'numeric'],
            'type' => ['required', 'in:soft,hard'],
        ]);
        if ($validation->fails()) {
            return response()->json($validation->errors(), Response::HTTP_NOT_ACCEPTABLE);
        }

        try {
            $product->update($request->all());
            $response = [
                'message' => 'Product updated!',
                'data' => $product,
            ];

            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed : " . $e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        try {
            $product->delete();
            $response = [
                'message' => 'Product deleted!',
            ];

            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed : " . $e->errorInfo
            ]);
        }
    }
}
